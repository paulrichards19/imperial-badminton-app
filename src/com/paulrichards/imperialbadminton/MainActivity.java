package com.paulrichards.imperialbadminton;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.paulrichards.imperialbadminton.R;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.os.Build;

public class MainActivity extends Activity {

		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		Log.e("Menu", "Error: " + id);
		if (id == R.id.action_settings) {
			return true;
		}
		
		if (id == R.id.action_website) {
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.imperialbadminton.co.uk/"));
			startActivity(browserIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		private MyJsonArrayAdapter jsonadaptor;
		
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			
			final ListView listview = (ListView) rootView.findViewById(R.id.listView1);
			
	        new MyAsyncTask(){
	        	
	    	    @Override
	    	    protected String getUrlSelect( ){
	    	    	return  "http://www.imperialbadminton.co.uk/index/data";
	    	    }        	
	        	
	      	    @Override
	    		protected void onPostExecute(Void v) {
	    			// TODO Auto-generated method stub
	    			Log.e("Info", "Result: Data returned from JSON" );
	    			 
	    	        //parse JSON data
	    	        try{
	    	           // JSONObject jObject = new JSONObject(result);
	    	            // extract data from meta
	    	            JSONArray jArray = new JSONArray(result); //jObject.getJSONArray("data");
	    	            
	    	            MyJsonArrayAdapter adapter3 = new MyJsonArrayAdapter(getActivity().getApplicationContext(), jArray){
	    	            	@Override
	    	            	public View getView(int position, View convertView, ViewGroup parent) {
	    	            		
	    	            		LayoutInflater inflater = (LayoutInflater) context
	    	            				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	            		View rowView = inflater.inflate(R.layout.activity_my_list_row, parent, false);
	    	            		
	    	            		TextView club = (TextView) rowView.findViewById(R.id.club);
	    	            		TextView league = (TextView) rowView.findViewById(R.id.league);
	    	            		TextView result = (TextView) rowView.findViewById(R.id.result);
	    	            		TextView homeAway = (TextView) rowView.findViewById(R.id.homeAway);
	    	            		TextView matchTime = (TextView) rowView.findViewById(R.id.matchTime);
	    	            		

	    	            		JSONObject json_data = (JSONObject)getItem(position);
	    	            		try {
	    	            			club.setText(json_data.getString("club"));
	    	            			league.setText(json_data.getString("league"));
	    	            			result.setText(json_data.getString("result"));
	    	            			homeAway.setText(json_data.getString("home_away"));
	    	            			
	    	            			Long millisecs = json_data.getLong("match_time") * 1000;
	    	            			
	    	            			Date date = new Date(millisecs);
	    	            			java.text.DateFormat dateFormat =
	    	            			    android.text.format.DateFormat.getDateFormat(getActivity().getApplicationContext());
	    	            			
	    	            			matchTime.setText( dateFormat.format(date));
	    	            			
	    	            			
	    	            		} catch (JSONException e) {
	    	            			// TODO Auto-generated catch block
	    	            			e.printStackTrace();
	    	            		}

	    	            		return rowView;
	    	            	}
	    	            }; 
	    	            
	    	            
	    	            listview.setAdapter(adapter3);
	    	            
	    	            //getActivity().jsonadaptor = adapter3;
	    	            
	    	        } catch (JSONException e) {

	    	            Log.e("JSONException", "Error: " + e.toString());

	    	        } // catch (JSONException e)    	    
	    			
	    		}
	        	  
	          }.execute("");
	
			
			return rootView;
		}
	}

}

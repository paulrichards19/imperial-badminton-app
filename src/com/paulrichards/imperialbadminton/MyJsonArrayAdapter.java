package com.paulrichards.imperialbadminton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyJsonArrayAdapter extends BaseAdapter {
	protected final Context context;
	private final JSONArray values;

	public MyJsonArrayAdapter(Context context, JSONArray values) {
		// super();
		this.context = context;
		this.values = values;
	}

	public JSONArray getValues(){
		
		return this.values;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return parent;
		/*
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.activity_my_list_row, parent, false);
		
		TextView nameSecurityLabel = (TextView) rowView.findViewById(R.id.name_security);
		TextView exchangeLabel = (TextView) rowView.findViewById(R.id.exchange);
		TextView lastPriceLabel = (TextView) rowView.findViewById(R.id.last_price);
		
		//JSONObject json_data = values.getJSONObject(position); values.g
		JSONObject json_data = (JSONObject)getItem(position);
		try {
			nameSecurityLabel.setText(json_data.getString("NAME"));
			exchangeLabel.setText(json_data.getString("EXCHANGE"));
			lastPriceLabel.setText(json_data.getString("PRICE"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		return rowView;
		*/
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return values.length();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		try {
			//get the data out
			
			return values.getJSONObject(position);
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
}
